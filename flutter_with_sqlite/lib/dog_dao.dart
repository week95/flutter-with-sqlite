import 'package:sqflite/sqflite.dart';

import 'database_provider.dart';
import 'dog.dart';

class DogDao {
  static Future<void> insertDog(Dog dog) async {
    final db = await DatabaseProvider.database;
    await db.insert(
      'dogs',
      dog.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }


  static Future<List<Dog>> dogs() async {
    final db = await DatabaseProvider.database;
    return Dog.toList(await db.query('dogs'));
  }


  static Future<void> updateDog(Dog dog) async {
    final db = await DatabaseProvider.database;

    await db.update(
      'dogs',
      dog.toMap(),
      where: 'id = ?',
      whereArgs: [dog.id],
    );
  }


  static Future<void> deleteDog(int id) async {
    final db = await DatabaseProvider.database;

    await db.delete(
      'dogs',
      where: 'id = ?',
      whereArgs: [id],
    );
  }

}
